from PostgreSQL.postgres_con import Execute
import Functions.global_ as global_
from Functions.AccessControl import AccessControl
import re, string


class DetailedView:
	def __init__(self):
		self.filtered_data1 = []
		self.data2 = []
		self.filterData()

	def filterData(self):
	    tablenames = str(AccessControl())
	    specialChars = ",\"()[]'" 
	    for specialChar in specialChars:
	      tablenames = tablenames.replace(specialChar, '')
	    list_tablenames= re.sub("[^\w]", " ",  tablenames).split()
	    data1 = Execute("Select * FROM " + global_.currtable + " LIMIT 0", 3)
	    substring = 'id_'+str(global_.currtable)
	    substring_in_list = any(substring in string for string in data1)
	    tables = []
	    alphabet = list(string.ascii_lowercase)
	    Sql = "SELECT * FROM " + global_.currtable +" "+ alphabet[0]
	    global_.count=1
	    tables.append(global_.currtable)
	    if substring_in_list == True :
	        for i in range(len(list_tablenames)):
	            columns = Execute("Select * FROM " + str(list_tablenames[i]) + " LIMIT 0", 3)
	            primaryKey = 'id_'+str(list_tablenames[i])
	            substring_in_columns = any(substring in string for string in columns)
	            primaryKey_in_columns = any(primaryKey in string for string in columns)
	            if substring_in_columns == True and primaryKey_in_columns == False and list_tablenames[i] != global_.currtable :
	                Sql = Sql + " JOIN " + list_tablenames[i] + " " + alphabet[global_.count] + " ON " + alphabet[global_.count]  + "." + substring + " = " + alphabet[0] +"." + substring
	                global_.count = global_.count + 1
	                tables.append(list_tablenames[i])
	                for i in range(len(list_tablenames)):
	                    key = "id_" + str(list_tablenames[i])
	                    if key in columns and  key != substring:
	                        Sql = Sql + " JOIN " + list_tablenames[i] + " " + alphabet[global_.count] + " ON " + alphabet[global_.count]  + "." + key + " = " + alphabet[global_.count - 1] +"." + key
	                        tables.append(list_tablenames[i])
	                        global_.count = global_.count + 1                
	            elif substring_in_columns == True and list_tablenames[i] != global_.currtable:
	                tables.append(list_tablenames[i])
	                Sql = Sql + " JOIN " + list_tablenames[i] + " " + alphabet[global_.count] + " ON " + alphabet[global_.count]  + "." + substring + " = " + alphabet[0] +"." + substring
	                global_.count = global_.count + 1

	        print(Sql)
	        specialChars = ",\"()[]'" 
	        for specialChar in specialChars:
	          tables = str(tables).replace(specialChar, '')
	        tables = tables.replace(' ', ',')
	        global_.count = 1
	        data1 = Execute("Select * FROM "+tables+" LIMIT 0", 3)
	        self.data2 = Execute(Sql, 2)
	        
	        global_.count = 0
	        for element in data1:
	            if element not in self.filtered_data1:
	                self.filtered_data1.append(element)
	                global_.count += 1
	            elif element in self.filtered_data1 and element == substring:
	               for i in range(len(self.data2)):
	                    y = list(self.data2[i])
	                    y.pop(global_.count)
	                    self.data2[i] = tuple(y)

	            elif element in self.filtered_data1 and element != substring:
	                if element in Sql:
	                    for i in range(len(self.data2)):
	                        y = list(self.data2[i])
	                        y.pop(global_.count)
	                        self.data2[i] = tuple(y)
	                else:
	                   nazev = str(element + "_2")
	                   self.filtered_data1.append(nazev)
	                   print(nazev)
	                   global_.count += 1
	    else:
	    	self.filtered_data1 = Execute("Select * FROM " + global_.currtable + " LIMIT 0", 3)
	    	self.data2 = Execute("Select * FROM "+global_.currtable, 2 )
