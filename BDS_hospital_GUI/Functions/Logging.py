from logging.config import dictConfig
import logging

def Logger(log_message):

    dictConfig({
        'version': 1,
        'formatters': {
            'standard': {
                'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
            }
        },
        'handlers': {
            'myapp_handler': {
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'filename': './logs/my_app.log',
                'when': 'd',
                'interval': 1,
                'backupCount': 30,
                'level': 'DEBUG',
                "encoding": "utf8",
                'formatter': 'standard'
            },
        },
        'loggers': {
            'simple': {
                'level': 'DEBUG',
                'handlers': ['myapp_handler']
            }
        },
    })


    logger = logging.getLogger("simple")

    logger.info(str(log_message))

