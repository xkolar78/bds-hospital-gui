from PostgreSQL.postgres_con import Execute
import bcrypt
import Functions.global_ as global_



def Login(username, password):
	db_password = Execute('SELECT password FROM users WHERE username = \'' + username + '\'',1)
	db_salt = Execute('SELECT salt FROM users WHERE username = \'' + username + '\'',1)
	
	if((db_salt != None) and (db_password != None)):
		hashed_password = bcrypt.hashpw(password.encode('utf-8'), db_salt.encode('utf-8'))
		if (hashed_password.decode() == db_password):
			global_.privilegies = Execute('SELECT user_privilegies FROM users WHERE username = \'' + username + '\'',1)
			global_.username = username
			return True
		else:
			return False
	else:
		return False
	
	