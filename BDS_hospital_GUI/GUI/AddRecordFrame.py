import Functions.global_ as global_
import tkinter as tk
from tkinter import font as tkfont
from tkinter import ttk
import Functions.global_
from PostgreSQL.postgres_con import Execute


class AddRecordFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        self.controller = controller
        
        self.entry_image_2 = tk.PhotoImage( file="./assets/entry_8.png")
    
    def addRecord(self):
        columns = ""
        columns_data = ""
        data_inputs = []

        for i in range(len(self.data)):
            input_name = "entry_"+ str(self.data[int(i)])
            data_inputs.append(eval("self." + input_name + ".get()")) 
            print(data_inputs)

        for i in range(len(self.data)):
            if columns != "":
                columns = columns + ","
                columns_data = columns_data + ","
            columns_data = columns_data + "'" + str(data_inputs[int(i)]) + "'"
            columns = columns + str(self.data[int(i)])

        Execute("INSERT INTO "+global_.currtable+"(" + columns + ") VALUES (" + columns_data + ")", 4)

        for i in range(len(self.data)):
            input_name = "entry_"+ str(self.data[int(i)])
            eval("self." + input_name + ".delete(0, 'end')")

    def returnMain(self):
        self.controller.showData()
        self.controller.show_frame("MainFrame")
        self.canvas.delete('all')

    def printBoxes(self):
        self.canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        self.canvas.place(x = 0, y = 0)
        self.image_image_1 = tk.PhotoImage(file="./assets/image_5.png")
        image_1 = self.canvas.create_image(450.0, 250.0, image=self.image_image_1 )
        self.button_image_1 = tk.PhotoImage(file="./assets/button_14.png")
        self.button_1 = tk.Button(self, image=self.button_image_1, borderwidth=0, highlightthickness=0, command=lambda: self.addRecord(), relief="flat") 
        self.button_1.place(x=605.0, y=376.0, width=180.0, height=55.0 )
        
        self.button_image_2 = tk.PhotoImage(file="./assets/button_4.png")
        button_2 = tk.Button(self, image=self.button_image_2, borderwidth=0, highlightthickness=0, command=lambda: self.returnMain() , relief="flat") 
        button_2.place(x=16.0, y=20.0, width=42.0, height=33.0 )

        self.data = Execute("Select * FROM "+global_.currtable+" LIMIT 0", 3)
        y = 55
        z = 75
        self.data = [x for x in self.data if not x.startswith('id_'+str(global_.currtable))]
        print(self.data)
        for  i  in range(len(self.data)):
            name = "entry_"+ str(self.data[int(i)])
            exec("self." + name + " =  " + "tk.Entry(self, bd=0, bg=\"#E5E5E5\", highlightthickness=0, font=(\"Roboto Bold\", 20 * -1))")
            self.text = self.canvas.create_text(120, int(y), anchor="nw", text=self.data[int(i)], fill="#222831", font=("Roboto Bold", 24 * -1))
            exec(name + " = 'something else'")
            eval("self."+ name + ".place(x=448.0, y=int("+ str(y) +"), width=221.0, height=36.0 )")
            self.canvas.create_image(558.5,int(z), image=self.entry_image_2)
            if y > 325:
                self.button_1.place(x=700.0, y=376.0, width=180.0, height=55.0 )
            y = y + 45
            z = z + 45
            
            