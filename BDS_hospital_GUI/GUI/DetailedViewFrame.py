import tkinter as tk
import re
from tkinter import font as tkfont
from tkinter import ttk
import Functions.global_ as global_
from PostgreSQL.postgres_con import Execute
from tkinter import messagebox
from Functions.DetailedView import DetailedView

class DetailedViewFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        self.canvas.place(x = 0, y = 0)
        self.image_6 = tk.PhotoImage(file="./assets/image_6.png")
        image_1 = self.canvas.create_image(450.0, 250.0, image=self.image_6 )
        self.button_image_2 = tk.PhotoImage(file="./assets/button_4.png")
        button_2 = tk.Button(self, image=self.button_image_2, borderwidth=0, highlightthickness=0, command=lambda: self.returnMain() , relief="flat") 
        button_2.place(x=1.0, y=1.0, width=42.0, height=33.0 )
        self.style = ttk.Style()
        self.style.configure("Treeview", background="#D3D3D3",foreground="black",rowheight=25,fieldbackground="#D3D3D3")
        self.style.map('Treeview',background=[('selected', "#7a7a7a")])
        self.tree_scroll = tk.Scrollbar(self)
        self.tree_scroll.pack()
        self.tree_scroll.place(x=800.0, y=33.0, width=20.0, height=420.0 )
        self.tree_scroll2 = ttk.Scrollbar(self,orient='horizontal')
        self.tree_scroll2.pack()
        self.tree_scroll2.place(x=90.0, y=455.0, width=700.0, height=20.0 )
        self.my_tree = ttk.Treeview(self, yscrollcommand=self.tree_scroll.set,xscrollcommand = self.tree_scroll2.set, selectmode="none")
        self.my_tree.pack()
        self.my_tree.place(x=90.0, y=33.0, width=700.0, height=420.0)
        self.tree_scroll.config(command=self.my_tree.yview)
        self.tree_scroll2.configure(command=self.my_tree.xview)

    def returnMain(self):
        self.controller.showData()
        self.controller.show_frame("MainFrame")
    
    def Function(self):
        return DetailedView()

    def showData(self):
        for record in self.my_tree.get_children():
            self.my_tree.delete(record)

        self.my_tree.tag_configure('oddrow', background="white")
        self.my_tree.tag_configure('evenrow', background="lightblue")
        for record in self.my_tree.get_children():
            self.my_tree.delete(record)
        
        f = self.Function()
        filtered_data1 = f.filtered_data1
        data2 = f.data2
        self.my_tree['columns'] = filtered_data1
        self.my_tree.column("#0", width=0, stretch= tk.NO)
        self.my_tree.heading("#0", text="", anchor=tk.W)

        for column in  filtered_data1:
            if len(filtered_data1) < 8 :
                width = 700 / len(filtered_data1)
            else:
                width = 90

            self.my_tree.column(column, anchor=tk.CENTER, width= int(width),stretch=False)
            self.my_tree.heading(column, text=column, anchor=tk.CENTER)
        
        global_.count = 0
        
        for  y  in range(len(data2)):
            
            if global_.count % 2 == 0:
                self.my_tree.insert(parent='', index='end', iid=global_.count, text="", values=(data2[y]), tags=('evenrow',))
            else:
                self.my_tree.insert(parent='', index='end', iid=global_.count, text="", values=(data2[y]), tags=('oddrow',))
            global_.count += 1

    

        
