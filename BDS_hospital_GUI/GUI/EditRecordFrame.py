import Functions.global_ as global_
import tkinter as tk
from tkinter import font as tkfont
from tkinter import ttk
import Functions.global_
from PostgreSQL.postgres_con import Execute


class EditRecordFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        self.controller = controller
        
        self.entry_image_2 = tk.PhotoImage( file="./assets/entry_8.png")
    
    def addRecord(self, values):
        toSQL = ""
        data_inputs = []
        data = [x for x in self.data if not x.startswith('id_'+str(global_.currtable))]

        for i in range(len(data)):
            input_name = "entry_"+ str(data[int(i)])
            data_inputs.append(eval("self." + input_name + ".get()"))
            toSQL = toSQL + str(data[i])+ "='" + data_inputs[i] + "',"

        toSQL = toSQL[:-1]
        
        Execute("UPDATE "+global_.currtable+" SET " + toSQL + " WHERE " + str(self.data[0]) + " =  '" + str(values[0]) + "'", 4)

        for i in range(len(data)):
            input_name = "entry_"+ str(data[int(i)])
            eval("self." + input_name + ".delete(0, 'end')")

    def returnMain(self):
        self.controller.showData()
        self.controller.show_frame("MainFrame")
        self.canvas.delete('all')

    def printBoxes(self, values):

        self.canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        self.canvas.place(x = 0, y = 0)
        self.image_image_1 = tk.PhotoImage(file="./assets/image_5.png")
        image_1 = self.canvas.create_image(450.0, 250.0, image=self.image_image_1 )
        self.button_image_1 = tk.PhotoImage(file="./assets/button_20.png")
        self.button_1 = tk.Button(self, image=self.button_image_1, borderwidth=0, highlightthickness=0, command=lambda: self.addRecord(values), relief="flat") 
        self.button_1.place(x=605.0, y=376.0, width=180.0, height=55.0 )
        
        self.button_image_2 = tk.PhotoImage(file="./assets/button_4.png")
        button_2 = tk.Button(self, image=self.button_image_2, borderwidth=0, highlightthickness=0, command=lambda: self.returnMain() , relief="flat") 
        button_2.place(x=16.0, y=20.0, width=42.0, height=33.0 )

        self.data = Execute("Select * FROM "+global_.currtable+" LIMIT 0", 3)
        y = 55
        z = 75
        substring = 'id_'+str(global_.currtable)
        substring_in_list = any(substring in string for string in self.data)
        if substring_in_list == True :
            data = [x for x in self.data if not x.startswith('id_'+str(global_.currtable))]
            treeData = values[1:]
            print("je primarni klic")
        else:
            data = self.data
            treeData = values
            print("nezacina")
        
        for  i  in range(len(data)):
            name = "entry_"+ str(data[int(i)])
            exec("self." + name + " =  " + "tk.Entry(self, bd=0, bg=\"#E5E5E5\", highlightthickness=0, font=(\"Roboto Bold\", 20 * -1))")
            self.text = self.canvas.create_text(120, int(y), anchor="nw", text=data[int(i)], fill="#222831", font=("Roboto Bold", 24 * -1))
            exec(name + " = 'something else'")
            eval("self."+ name + ".place(x=448.0, y=int("+ str(y) +"), width=221.0, height=36.0 )")
            self.canvas.create_image(558.5,int(z), image=self.entry_image_2)
            if y > 325:
                self.button_1.place(x=700.0, y=376.0, width=180.0, height=55.0 )
            y = y + 45
            z = z + 45
            exec("self." + name + ".insert(0,\"" + str(treeData[i]) + "\")")
            
            