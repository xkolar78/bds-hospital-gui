import tkinter as tk
from tkinter import font as tkfont
from tkinter import ttk
import Functions.global_ as global_
from PostgreSQL.postgres_con import Execute
from tkinter import messagebox
from Functions.AccessControl import AccessControl
from Functions.CreateDummyTable import create_dummy_table

class MainFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        self.canvas.place(x = 0, y = 0)
        self.canvas.create_rectangle(777.0, 0.0, 900.0, 500.0, fill="#FCFCFC", outline="")
        self.canvas.create_text(50.0, 3.0, anchor="nw", text="Table: ", fill="#FFFFFF", font=("Roboto Bold", 24 * -1) )
        self.canvas.create_text(370.0, 3.0, anchor="nw", text="Find by: ", fill="#FFFFFF", font=("Roboto Bold", 24 * -1) )
        self.button_image_1 = tk.PhotoImage(file="./assets/button_7.png")
        self.find_input = tk.Entry(self, bd=0, bg="#FFFFFF", highlightthickness=0, font=("Roboto Bold", 13 * -1), justify='center')
        self.find_input.place(x = 570, y = 5 ,width=105.0, height=25.0)
        button_1 = tk.Button(self, image=self.button_image_1, borderwidth=0, highlightthickness=0, command=lambda: self.CreateDummyTable(), relief="flat") 
        button_1.place(x=786.0, y=392.0, width=105.0, height=44.0 )
        self.button_image_2 = tk.PhotoImage(file="./assets/button_8.png")
        button_2 = tk.Button(self, image=self.button_image_2, borderwidth=0, highlightthickness=0, command=lambda: self.addNewRecord(), relief="flat") 
        button_2.place(x=786.0, y=18.0, width=105.0, height=44.0 )     
        self.button_image_4 = tk.PhotoImage(file="./assets/button_10.png")
        button_4 = tk.Button(self, image=self.button_image_4, borderwidth=0, highlightthickness=0, command=lambda: self.controller.show_frame("LoginFrame"), relief="flat") 
        button_4.place(x=787.0, y=445.0, width=105.0, height=44.0 )
        self.button_image_5 = tk.PhotoImage(file="./assets/edit_button.png")
        button_5 = tk.Button(self, image=self.button_image_5, borderwidth=0, highlightthickness=0, command=lambda: self.editRecord(), relief="flat") 
        button_5.place(x=786.0, y=70.0, width=105.0, height=44.0 )
        self.button_image_6 = tk.PhotoImage(file="./assets/Button_search.png")
        button_6 = tk.Button(self, image=self.button_image_6, borderwidth=0, highlightthickness=0, command=lambda: self.searchData(), relief="flat") 
        button_6.place(x=685, y=5.0, width=75.0, height=25.0 )
        self.button_image_7 = tk.PhotoImage(file="./assets/button_11.png")
        button_7 = tk.Button(self, image=self.button_image_7, borderwidth=0, highlightthickness=0, command=lambda: self.deleteRecord(), relief="flat") 
        button_7.place(x=786.0, y=122.0, width=105.0, height=44.0 )
        self.button_image_8 = tk.PhotoImage(file="./assets/button_21.png")
        button_8 = tk.Button(self, image=self.button_image_8, borderwidth=0, highlightthickness=0, command=lambda: self.detailedView(), relief="flat") 
        button_8.place(x=786.0, y=174.0, width=105.0, height=44.0 )
        self.button_image_9 = tk.PhotoImage(file="./assets/button_22.png")
        button_9 = tk.Button(self, image=self.button_image_9, borderwidth=0, highlightthickness=0, command=lambda: self.InjectionFrame(), relief="flat") 
        button_9.place(x=786.0, y=226.0, width=105.0, height=44.0 )
        self.style = ttk.Style()
        self.style.configure("Treeview", background="#D3D3D3",foreground="black",rowheight=25,fieldbackground="#D3D3D3")
        self.style.map('Treeview',background=[('selected', "#7a7a7a")])
        self.tree_scroll = tk.Scrollbar(self)
        self.tree_scroll.pack()
        self.tree_scroll.place(x=740.0, y=37.0, width=20.0, height=420.0 )
        self.my_tree = ttk.Treeview(self, yscrollcommand=self.tree_scroll.set, selectmode="extended")
        self.my_tree.pack()
        self.my_tree.place(x=37.0, y=37.0, width=700.0, height=420.0 )
        self.tree_scroll.config(command=self.my_tree.yview)
        self.ComboBox = ttk.Combobox(self, values= "",state="readonly",justify='center',font=('Helvetica',8))
        self.ComboBox.place(x=128.0, y=5.0, width=150.0, height=25.0)
        self.ComboBox.bind("<<ComboboxSelected>>", lambda _ : self.showData())
        self.ComboBox2 = ttk.Combobox(self, values= "",state="readonly",justify='center',font=('Helvetica',8))
        self.ComboBox2.place(x=465.0, y=5.0, width=100.0, height=25.0)

    def CreateDummyTable(self):
        create_dummy_table()
        self.fillComboBox()

        
    def InjectionFrame(self):
        x = int(self.my_tree.selection()[0])
        values = []
        for value in self.my_tree.item(x)['values']:
            values.append(value)
        self.controller.printBoxes_Injection(values)
        self.controller.show_frame("InjectionFrame")


    def fillComboBox(self):
        tablenames = AccessControl()
        self.ComboBox.configure(values = tablenames)
        self.ComboBox.current(0)
        columns = Execute("Select * FROM "+self.ComboBox.get()+" LIMIT 0", 3)
        self.ComboBox2.configure(values = columns)
        self.ComboBox2.current(0)


    def detailedView(self):
        self.controller.detailedView()
        self.controller.show_frame("DetailedViewFrame")   


    def deleteRecord(self):
        x = int(self.my_tree.selection()[0])
        print(x)
        values = []
        for value in self.my_tree.item(x)['values']:
            values.append(value)
        answer = messagebox.askquestion("Delete Record", "Are you sure you want to delete record?")
        if answer == "yes":
            data1 = Execute("Select * FROM "+self.ComboBox.get()+" LIMIT 0", 3)
            name = Execute("DELETE FROM "+ self.ComboBox.get()+" WHERE "+str(data1[0])+"='"+str(values[0])+"'", 4)
            self.showData()

    def showData(self):
        for record in self.my_tree.get_children():
            self.my_tree.delete(record)
        self.my_tree.tag_configure('oddrow', background="white")
        self.my_tree.tag_configure('evenrow', background="lightblue")
        global_.count=0
        
        data1 = Execute("Select * FROM "+self.ComboBox.get()+" LIMIT 0", 3)

        self.my_tree['columns'] = data1
        
        self.my_tree.column("#0", width=0, stretch=tk.NO)
        self.my_tree.heading("#0", text="", anchor=tk.W)

        for column in  data1:
            width = 700 / len(data1)
            self.my_tree.column(column, anchor=tk.CENTER, width= int(width))
            self.my_tree.heading(column, text=column, anchor=tk.CENTER)
        data2 = Execute("Select * FROM "+self.ComboBox.get()+" ORDER BY "+ data1[0] , 2)
        global_.currtable = self.ComboBox.get()
        for  y  in range(len(data2)):
            
            if global_.count % 2 == 0:
                self.my_tree.insert(parent='', index='end', iid=global_.count, text="", values=(data2[y]), tags=('evenrow',))
            else:
                self.my_tree.insert(parent='', index='end', iid=global_.count, text="", values=(data2[y]), tags=('oddrow',))
            global_.count += 1

        self.ComboBox2.configure(values = data1)
        self.ComboBox2.current(0)

    def searchData(self):
        if self.find_input.get() != "":
            for record in self.my_tree.get_children():
                self.my_tree.delete(record)
            self.my_tree.tag_configure('oddrow', background = "white")
            self.my_tree.tag_configure('evenrow', background = "lightblue")

            for record in self.my_tree.get_children():
                self.my_tree.delete(record)
            global_.count = 0
            data2 = Execute("Select * FROM " + self.ComboBox.get() + " WHERE " + self.ComboBox2.get() + "= '" + self.find_input.get() + "'", 2)
            for y in range(len(data2)):

                if global_.count % 2 == 0:
                   self.my_tree.insert(parent = '', index = 'end', iid = global_.count, text = "", values = (data2[y]), tags = ('evenrow', ))
                else :
                    self.my_tree.insert(parent = '', index = 'end', iid = global_.count, text = "", values = (data2[y]), tags = ('oddrow', ))
                global_.count += 1
        else:
            self.showData()

    def editRecord(self):
        x = int(self.my_tree.selection()[0])
        values = []
        for value in self.my_tree.item(x)['values']:
            values.append(value)
        self.controller.printBoxes_Edit(values)
        self.controller.show_frame("EditRecordFrame")

    def addNewRecord(self):
        self.controller.printBoxes()
        self.controller.show_frame("AddRecordFrame")

