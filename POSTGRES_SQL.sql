CREATE TABLE IF NOT EXISTS address
(
    id_address BIGSERIAL NOT NULL,
    city VARCHAR(45) NOT NULL,
    house_number VARCHAR(45) NOT NULL,
    street VARCHAR(45) NOT NULL,
    zip_code VARCHAR(45) NOT NULL,
    PRIMARY KEY (id_address)
);

CREATE TABLE IF NOT EXISTS building
(
    id_building SERIAL NOT NULL,
    building_type VARCHAR(45) NOT NULL,
    building_description VARCHAR(45) NOT NULL,
    PRIMARY KEY (id_building)
);

CREATE TABLE IF NOT EXISTS dosing_list
(
    id_dosing_list SERIAL NOT NULL,
    id_patient integer NOT NULL,
    PRIMARY KEY (id_dosing_list)
);

CREATE TABLE IF NOT EXISTS dosing_to_list
(
    dosing_value VARCHAR(150) NOT NULL,
    dose_description VARCHAR(200),
    dose_time VARCHAR(100) NOT NULL,
    id_medication integer,
    id_dosing_list integer NOT NULL
);

CREATE TABLE IF NOT EXISTS employee
(
    id_employee BIGSERIAL NOT NULL,
    given_name VARCHAR(45) NOT NULL,
    family_name VARCHAR(45) NOT NULL,
    mail VARCHAR(45) NOT NULL,
    phone_number integer NOT NULL,
    photo VARCHAR(45) NOT NULL,
    PRIMARY KEY (id_employee)
);
CREATE TABLE IF NOT EXISTS dummy_table
(
    id_dummy_table SERIAL NOT NULL,
    given_name VARCHAR(45) NOT NULL,
    family_name VARCHAR(45) NOT NULL,
    PRIMARY KEY (id_dummy_table)
);

CREATE TABLE IF NOT EXISTS users
(
	id_user BIGSERIAL NOT NULL,
    username VARCHAR(45) NOT NULL,
    password VARCHAR(100) NOT NULL,
    salt VARCHAR(100) NOT NULL,
    id_employee integer NOT NULL,
    user_privilegies VARCHAR(45) NOT NULL,
    PRIMARY KEY (id_user)
);

CREATE TABLE IF NOT EXISTS employee_has_address
(
    id_address integer,
    id_employee integer
);

CREATE TABLE IF NOT EXISTS medication
(
    id_medication SERIAL NOT NULL,
    medication_type VARCHAR(50) NOT NULL,
    medication_description VARCHAR(200) NOT NULL,
    recommended_dose_value VARCHAR(150) NOT NULL,
    PRIMARY KEY (id_medication)
);

CREATE TABLE IF NOT EXISTS patient
(
    id_patient BIGSERIAL NOT NULL,
    given_name VARCHAR(45) NOT NULL,
    family_name VARCHAR(45) NOT NULL,
    phone_number integer NOT NULL,
    family_contact integer NOT NULL,
    photo VARCHAR(45),
    PRIMARY KEY (id_patient)
);

CREATE TABLE IF NOT EXISTS patient_has_address
(
    id_patient integer,
    id_address integer
);

CREATE TABLE IF NOT EXISTS "position"
(
    id_employee integer,
    position_value VARCHAR(45) NOT NULL
);

CREATE TABLE IF NOT EXISTS procedure
(
    id_procedure BIGSERIAL NOT NULL,
    id_room integer NOT NULL,
    date VARCHAR(15) NOT NULL,
    time_start VARCHAR(45) NOT NULL,
    procedure_type VARCHAR(45) NOT NULL,
    topic VARCHAR(45) NOT NULL,
    id_patient integer,
    id_employee integer,
    PRIMARY KEY (id_procedure)
);

CREATE TABLE IF NOT EXISTS room
(
    id_room BIGSERIAL NOT NULL,
    id_building integer,
    floor integer NOT NULL,
    room_number integer NOT NULL,
    PRIMARY KEY (id_room)
);

CREATE TABLE IF NOT EXISTS stay
(
    id_stay BIGSERIAL NOT NULL,
    id_patient integer,
    id_room integer,
    start_date VARCHAR(45) NOT NULL,
    end_date VARCHAR(45) NOT NULL,
    PRIMARY KEY (id_stay)
);

ALTER TABLE dosing_list
    ADD FOREIGN KEY (id_patient)
    REFERENCES patient (id_patient)
    ;


ALTER TABLE dosing_to_list
    ADD FOREIGN KEY (id_medication)
    REFERENCES medication (id_medication)
    ;


ALTER TABLE dosing_to_list
    ADD FOREIGN KEY (id_dosing_list)
    REFERENCES dosing_list (id_dosing_list)
    ;


ALTER TABLE employee_has_address
    ADD FOREIGN KEY (id_address)
    REFERENCES address (id_address)
    ;


ALTER TABLE employee_has_address
    ADD FOREIGN KEY (id_employee)
    REFERENCES employee (id_employee)
    ;


ALTER TABLE patient_has_address
    ADD FOREIGN KEY (id_address)
    REFERENCES address (id_address)
    ;


ALTER TABLE patient_has_address
    ADD FOREIGN KEY (id_patient)
    REFERENCES patient (id_patient)
    ;


ALTER TABLE "position"
    ADD FOREIGN KEY (id_employee)
    REFERENCES employee (id_employee)
    ;


ALTER TABLE procedure
    ADD FOREIGN KEY (id_employee)
    REFERENCES employee (id_employee)
    ;


ALTER TABLE procedure
    ADD FOREIGN KEY (id_patient)
    REFERENCES patient (id_patient)
    ;


ALTER TABLE procedure
    ADD FOREIGN KEY (id_room)
    REFERENCES room (id_room)
    ;


ALTER TABLE room
    ADD FOREIGN KEY (id_building)
    REFERENCES building (id_building)
    ;


ALTER TABLE stay
    ADD FOREIGN KEY (id_patient)
    REFERENCES patient (id_patient)
    ;


ALTER TABLE stay
    ADD FOREIGN KEY (id_room)
    REFERENCES room (id_room)
    ;

ALTER TABLE users
    ADD FOREIGN KEY (id_employee)
    REFERENCES employee (id_employee)
    ;


INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Osek', '72', 'Františka Černého', '229 87');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Rýmařov', '478', 'Před Rybníkem', '596 85');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Pečky', '5', 'Juarézova', '462 52');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Rýmařov', '82', 'Poleradská', '761 81');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Chomutov', '2', 'Tanvaldská', '136 55');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Kardašova Řečice', '96', 'U Waltrovky', '427 93');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Chvaletice', '7', 'Rokytnická', '623 17');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Kdyně', '16', 'Spinozova', '587 27');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Jablonec nad Jizerou', '40', 'K Hořavce', '539 48');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Rousínov', '5', 'U Hostivařského Nádraží', '545 04');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Horní Blatná', '442', 'Musílkova', '496 63');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Pacov', '6', 'Vratislavova', '473 16');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Hoštka', '73', 'Tachovské Náměstí', '221 69');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Přerov', '130', 'Anny Drabíkové', '116 82');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Jablonec nad Jizerou', '318', 'Dřítenská', '720 30');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Jablonec nad Jizerou', '11', 'Na Příčné Mezi', '348 25');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Náměšť nad Oslavou', '759', 'Meziškolská', '116 71');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Kralovice', '578', 'Na Šafránce', '709 53');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Kravaře', '12', 'K Vidouli', '173 29');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Olomouc', '55', 'Sitteho', '399 11');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Olomouc', '1', 'Anny Drabíkové', '428 00');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Mašťov', '460', 'K Dálnici', '732 43');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Mýto', '6', 'Za Stadionem', '484 37');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Kamenice nad Lipou', '275', 'Čankovská', '463 98');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Žlutice', '431', 'V Nížinách', '235 06');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Kouřim', '54', 'Severní Viii', '723 49');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Lanžhot', '207', 'Maňákova', '653 72');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Mirovice', '6', 'Slatiňanská', '189 71');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Planá nad Lužnicí', '385', 'Paťanka', '200 71');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Valtice', '44', 'Pohořelec', '113 40');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Žebrák', '909', 'U Nové Dálnice', '713 02');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Vimperk', '85', 'Josefa Šimůnka', '634 14');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Sázava', '64', 'Plynární', '357 09');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Luhačovice', '991', 'K Přehradám', '205 44');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Dašice', '377', 'Ostrovského', '170 12');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Rtyně v Podkrkonoší', '7', 'Jihovýchodní I', '332 61');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Přimda', '398', 'Budapešťská', '590 93');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Liberec', '183', 'Šachovská', '744 25');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Horažďovice', '78', 'Na Šubě', '314 84');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Třemošnice', '704', 'K Vidouli', '329 44');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Libčice nad Vltavou', '57', 'Do Zahrádek Ii', '186 30');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Vlachovo Březí', '1', 'Horní Chaloupky', '475 89');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Mimoň', '379', 'Dobrošovská', '628 49');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Valašské Klobouky', '24', 'Heydukova', '291 90');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Žandov', '6', 'Jilmová', '650 89');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Benešov nad Ploučnicí', '14', 'V Padolině', '753 58');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Zákupy', '322', 'K Betáni', '206 53');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Moravský Beroun', '8', 'Ke Břvům', '783 54');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Rudolfov', '1', 'Za Valem', '417 80');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Česká Skalice', '18', 'Nad Krocínkou', '749 90');
INSERT INTO address ( city, house_number, street, zip_code) VALUES ( 'Týnec nad Sázavou', '339', 'Augustova', '528 11');

INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'paralen', 'proti bolesti', '2 tablety');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'ibalgin', 'tlumeni bolesti', '1 tableta');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'xanax', 'antidepresivum', 'polovina tablety');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'zenaro', 'proti alergii', '1x denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'actos', 'náhrada za metaformin(cukrovka)', '15-30mg denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Actrapid', 'inzulin', 'injekčně 1x ampulka');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Artrodar', 'proti zánětům na klouby', '2 toboly 50mg denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Aryzalera', 'maniodeprese a schizofrenie', '15-30mg 1x denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Aspulmo', 'uvolnění průdušek', 'inhalace 1-2 vdechů nárazově při záchvatu');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Atram', 'snížení tlaku a zklidnění', '2x denně od 6,25 do 25mg');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Axetine', 'streptokokové infekce dýchacích cest', '3x denně roztokxinfuze 750x1500mg');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Bisocard', 'snižuje krevní tlak a stres, proti srdeční poruše rytmu', '1x denně 2,5-10mg');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Brieka', 'proti epileptickým záchvatům', '150-600mg ve 3 dávkách denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Budenofalk', 'střevní záněty', '9mg v podání rektální pěna pommocí aplikátoru');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Bupainx', 'opioidní anelgetikum', '1 náplast interval výměny 3-4 dny');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Caduet', 'snížení cholesterolu', '5-10mg denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Calgel', 'lokální anestetikum na dásně a afty', 'několikrát denně vmasírovat do dutiny ústní');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Canocord', 'snížení tlaku po infarktecha srdečních příhodách', '1x denně 4-32mg nepřevyšovat maximální dávku!');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Carbomedac', 'zhoubné nádory vaječníků a plic', 'pomalá nitrožilní infuze - rozhoduje onkolog');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Coryol', 'snížení tlaku a bušení srdce', '2 tobolky denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Dacogen', 'akutní leukemie', 'nitrožilně, roztok dle onkologa');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Daylette', 'zabránění nechtěnému otěhotnění', '3 týdny 1 tableta denně 1 týden pauza');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Desloratadine', 'alergická rýma a kopřivka', '5mg denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Diecyclen', 'zabránění nechtěnému otěhotnění', '3 týdny 1 tableta denně 1 týden pauza');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Dobutamin', 'zvýšení krevního tlaku', '2-10 mikrogramů na 1kg tělesé hmoty');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Elidel', 'chronické záněty kůže', 'krém 2x denně na poostižená místa');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Egolanza', 'léčba schizofrenie a maniodeprese', '5-20mg denně dle pacienta');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Encephabol', 'prokrvení mozku a zlepšení paměti', '300-600mg denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Erclany', 'proti epileptickým záchvatům', '150-600mg ve 3 dávkách denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Eucarbon', 'léčba zácpy', '1-2 tablety 2-3x denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Fenofix', 'snížení hladiny tuků', '1 tableta denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Fluoxetin', 'poruchy nálady', '1 tableta 1x denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Fyssagir', 'zabránění nechtěnému otěhotnění', '3 týdny 1 tableta denně 1 týden pauza');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Gendron', 'osteopróza a řídnutí kostí', '1 tableta 1x denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Gliclazide', 'proti cukrovce', '30mg denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Gutron', 'zvýšení krevního tlaku', '2,5mg 2x denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Hartil-H', 'zmenšení krevního tlaku', '1 tabletka 1x denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Helicid', 'sklidnění žaludku', '20-40mg denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Hyalgan', 'boest kloubů', 'injekčně do kloubu 1x denně po dobu 5 týdnů');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Hypnomidate', 'celková anestézie', 'rozhoduje lekař');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Ibandronát', 'proti řídnutí kostí', '150mg denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Ibument', 'proti bolesti', '2 tebletky 2x denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Ichtoxyl', 'proti ekzémům', 'potírat 3x denně postižená místa');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Ilibrift', 'pro zpomalení srdce', '5mg 2x denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Imunor', 'poruchy imunity', '4-6 lahviček v týdenních intervalech');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Iprofenex', 'proti bolesti', '2 tablety 1x denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Litak', 'léčba leukémie', 'injekčně dle lékaře');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Megace', 'zvýšení chuti k jídlu', '600-800mg denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Migraptan', 'proti stresu', '50mg denně');
INSERT INTO medication ( medication_type, medication_description, recommended_dose_value) VALUES ( 'Neupogen', 'zvýšení bílých krvinek', '1x injekčně denně');


INSERT INTO building ( building_type, building_description) VALUES ( 'dětská nemocnice', 'růžová budova');
INSERT INTO building ( building_type, building_description) VALUES ( 'porodnice', 'zelená budova');
INSERT INTO building ( building_type, building_description) VALUES ( 'chirurgie', 'modrá budova');
INSERT INTO building ( building_type, building_description) VALUES ( 'plastická chirurgie', 'žlutá budova');
INSERT INTO building ( building_type, building_description) VALUES ( 'jednotka intenzivní péče', 'červená budova');

INSERT INTO room ( id_building, floor, room_number) VALUES ( 1, 1, 101);
INSERT INTO room ( id_building, floor, room_number) VALUES ( 1, 2, 102);
INSERT INTO room ( id_building, floor, room_number) VALUES ( 2, 1, 201);
INSERT INTO room ( id_building, floor, room_number) VALUES ( 2, 2, 202);
INSERT INTO room ( id_building, floor, room_number) VALUES ( 3, 1, 301);
INSERT INTO room ( id_building, floor, room_number) VALUES ( 3, 2, 302);
INSERT INTO room ( id_building, floor, room_number) VALUES ( 4, 1, 401);
INSERT INTO room ( id_building, floor, room_number) VALUES ( 4, 2, 402);
INSERT INTO room ( id_building, floor, room_number) VALUES ( 5, 1, 501);
INSERT INTO room ( id_building, floor, room_number) VALUES ( 5, 2, 502);

INSERT INTO patient ( given_name, family_name, phone_number, family_contact, photo) VALUES ( 'Adam', 'Velký', 456954875, 146789546, 'photo.url');
INSERT INTO patient ( given_name, family_name, phone_number, family_contact, photo) VALUES ( 'Lukáš', 'Petrášek', 741852963, 756654456, 'photo.url');
INSERT INTO patient ( given_name, family_name, phone_number, family_contact, photo) VALUES ( 'Daniel', 'Legner', 123456789, 852369456, 'photo.url');
INSERT INTO patient ( given_name, family_name, phone_number, family_contact, photo) VALUES ( 'Ondřej', 'Hradil', 951357852, 212345682, 'photo.url');
INSERT INTO patient ( given_name, family_name, phone_number, family_contact, photo) VALUES ( 'Miloslav', 'Kroneisl', 777536945, 123456789, 'photo.url');

INSERT INTO dosing_list ( id_patient) VALUES (2);
INSERT INTO dosing_list ( id_patient) VALUES (1);
INSERT INTO dosing_list ( id_patient) VALUES (4);
INSERT INTO dosing_list ( id_patient) VALUES (3);
INSERT INTO dosing_list ( id_patient) VALUES (5);



INSERT INTO dosing_to_list (dosing_value, dose_description, dose_time, id_medication, id_dosing_list) VALUES ('5mg', '5mg rozděleno do dvou dávek nitrožilně 2,5mg ráno a 2,5mg večer', '10:00, 18:00', 36, 1);
INSERT INTO dosing_to_list (dosing_value, dose_description, dose_time, id_medication, id_dosing_list) VALUES ('20ml', 'roztírat na postižená místa 3x denně', '08:00, 12:00, 18:00', 43, 2);
INSERT INTO dosing_to_list (dosing_value, dose_description, dose_time, id_medication, id_dosing_list) VALUES ('50ml', 'vnitrožilně 2x 25ml denně', '11:00, 23:00', 47, 3);
INSERT INTO dosing_to_list (dosing_value, dose_description, dose_time, id_medication, id_dosing_list) VALUES ('650mg', '3x denně (200mg, 200mg, 250mg)', '04:00, 12:00, 21:00', 13, 4);
INSERT INTO dosing_to_list (dosing_value, dose_description, dose_time, id_medication, id_dosing_list) VALUES ('1 tableta', '1x tableta denně po dobu 3 týdnů, týden pauza', '18:00', 22, 5);




INSERT INTO employee ( given_name, family_name, mail, phone_number, photo) VALUES ( 'Marek', 'Vávra', 'marekvavra@seznam.cz', 456584215, 'photo.url');
INSERT INTO employee ( given_name, family_name, mail, phone_number, photo) VALUES ( 'Ondřej', 'Kodl', 'ondrejkodl@seznam.cz', 741258963, 'photo.url');
INSERT INTO employee ( given_name, family_name, mail, phone_number, photo) VALUES ( 'Hugo', 'Toxxx', 'hugotoxxx@seznam.cz', 753951258, 'photo.url');
INSERT INTO employee ( given_name, family_name, mail, phone_number, photo) VALUES ( 'Radim', 'Ošklivý', 'radimosklivy@seznam.cz', 789456123, 'photo.url');
INSERT INTO employee ( given_name, family_name, mail, phone_number, photo) VALUES ( 'Jan', 'Novák', 'jannovak@seznam.cz', 452125478, 'photo.url');




INSERT INTO employee_has_address (id_address, id_employee) VALUES (8, 1);
INSERT INTO employee_has_address (id_address, id_employee) VALUES (27, 2);
INSERT INTO employee_has_address (id_address, id_employee) VALUES (35, 3);
INSERT INTO employee_has_address (id_address, id_employee) VALUES (29, 4);
INSERT INTO employee_has_address (id_address, id_employee) VALUES (50, 5);


INSERT INTO patient_has_address (id_patient, id_address) VALUES (1, 5);
INSERT INTO patient_has_address (id_patient, id_address) VALUES (2, 7);
INSERT INTO patient_has_address (id_patient, id_address) VALUES (3, 15);
INSERT INTO patient_has_address (id_patient, id_address) VALUES (4, 37);
INSERT INTO patient_has_address (id_patient, id_address) VALUES (5, 45);


INSERT INTO "position" (id_employee, position_value) VALUES (1, 'primář');
INSERT INTO "position" (id_employee, position_value) VALUES (2, 'chirurg');
INSERT INTO "position" (id_employee, position_value) VALUES (3, 'pomocná sestra');
INSERT INTO "position" (id_employee, position_value) VALUES (4, 'řidič sanitky');
INSERT INTO "position" (id_employee, position_value) VALUES (5, 'správce');


INSERT INTO procedure ( id_room, date, time_start, procedure_type, topic, id_patient, id_employee) VALUES (6, '2021-10-31', '12:00', 'transplantace srdce', '', 1, 1);
INSERT INTO procedure ( id_room, date, time_start, procedure_type, topic, id_patient, id_employee) VALUES (5, '2021-11-11', '08:00', 'plastické zvětšení prsou', 'velikost H', 5, 2);
INSERT INTO procedure ( id_room, date, time_start, procedure_type, topic, id_patient, id_employee) VALUES (5, '2021-11-13', '14:00', 'zmenšení nosu', '', 2, 1);
INSERT INTO procedure ( id_room, date, time_start, procedure_type, topic, id_patient, id_employee) VALUES (6, '2021-12-25', '15:00', 'katetrizace srdce', '', 4, 2);
INSERT INTO procedure ( id_room, date, time_start, procedure_type, topic, id_patient, id_employee) VALUES (3, '2022-01-01', 'dle situace', 'porod', 'dvojčata, císařský řez', 3, 2);


INSERT INTO stay ( id_patient, id_room, start_date, end_date) VALUES ( 5, 2, '28.10.2021', '05.11.2021');
INSERT INTO stay ( id_patient, id_room, start_date, end_date) VALUES ( 4, 1, '27.10.2021', '06.11.2021');
INSERT INTO stay ( id_patient, id_room, start_date, end_date) VALUES ( 3, 4, '18.09.2021', '05.10.2021');
INSERT INTO stay ( id_patient, id_room, start_date, end_date) VALUES ( 1, 3, '15.09.2021', '15.12.2021');
INSERT INTO stay ( id_patient, id_room, start_date, end_date) VALUES ( 2, 5, '01.06.2021', '01.01.2022');

INSERT INTO users (username, password, salt, id_employee, user_privilegies) VALUES ('employee', '$2b$12$wzrB4DDT4C01qDK/BIwA4.LxqAtasY5pic77wkntCT3GyYfY62n86', '$2b$12$wzrB4DDT4C01qDK/BIwA4.', 2, 'employee');
